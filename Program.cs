﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace maze_gen
{
    

    interface IMazeAlgorithm
    {
        string GenerateMaze();
    }

    class DepthFirstAlgorithm : IMazeAlgorithm
    {
        protected Random _random = new Random();

        public class Cell
        {
            public const string NO_CELL = "<none>";
            public string Label {get;set;}
            protected Cell _cellNorth;
            protected Cell _cellSouth;
            protected Cell _cellEast;
            protected Cell _cellWest;

            public Boolean? HasWall(Cell a) => walls.GetValueOrDefault((this, a));

            public Cell CellNorth { 
                get { return _cellNorth; } 
                set { 
                    if (value != _cellNorth) {
                        _cellNorth = value; 
                        value.CellSouth = this;  
                        CreateWall(_cellNorth);
                    }
                } 
            }
            public Cell CellSouth { 
                get { return _cellSouth; } 
                set { 
                    if (value != _cellSouth) {
                        _cellSouth = value;
                        value.CellNorth = this;
                        CreateWall(_cellSouth);
                    }  
                } 
            }
            public Cell CellEast { 
                get { return _cellEast; } 
                set { 
                    if (value != _cellEast) {
                        _cellEast = value;
                        value.CellWest = this;
                        CreateWall(_cellEast);
                    } 
                }
            }
            public Cell CellWest 
            { 
                get { return _cellWest; } 
                set { 
                    if (value != _cellWest) {
                        _cellWest = value;
                        value.CellEast = this;
                        CreateWall(_cellWest);
                    } 
                }
            }

            public void CreateWall(Cell other) {
                //Debug.Assert(this.HasWall(other) == other.HasWall(this));
                if (walls.GetValueOrDefault((this, other)) != true)
                {
                    walls[(this, other)] = true;
                    other.CreateWall(this);
                }
                Debug.Assert(this.HasWall(other) == other.HasWall(this));
            }
            public void RemoveWall(Cell other) {
                //Debug.Assert(this.HasWall(other) == other.HasWall(this));
                if (walls.GetValueOrDefault((this, other)) != false)
                {    
                    walls[(this, other)] = false;
                    other.RemoveWall(this);
                }
                Debug.Assert(this.HasWall(other) == other.HasWall(this));
            }

            protected Dictionary<(Cell, Cell), bool?> walls = new Dictionary<(Cell, Cell), bool?>();

            public Cell()
            {
            }

            public override string ToString() => $"N: {CellNorth?.Label ?? NO_CELL}; E: {CellEast?.Label ?? NO_CELL}; S: {CellSouth?.Label ?? NO_CELL}; W: {CellWest?.Label ?? NO_CELL};";
        }


        public Dictionary<(int, int), Cell> CreateMazeCells(int rows, int cols)
        {
            var cellLookup = new Dictionary<(int, int), Cell>();
            
            for (var r = 0; r < rows; ++r)
            {
                for (var c = 0; c < cols; ++c)
                {
                    var cell = new Cell { Label = $"({r}, {c})"};

                    cellLookup[(r, c)] = cell;

                    //  find neighbours (if they exist)
                    cell.CellNorth = cellLookup.GetValueOrDefault((r-1, c));
                    cell.CellSouth = cellLookup.GetValueOrDefault((r+1, c));
                    cell.CellWest = cellLookup.GetValueOrDefault((r, c-1));
                    cell.CellEast = cellLookup.GetValueOrDefault((r, c+1));
                }
            }

            return cellLookup;
        }

        protected IEnumerable<(int, int)> GetNeighbours((int, int) index, Dictionary<(int, int), Cell> cells)
        {
            (int, int)[] neighbourIndices = {
                (index.Item1-1, index.Item2),
                (index.Item1+1, index.Item2),
                (index.Item1, index.Item2-1),
                (index.Item1, index.Item2+1),
            };
            return neighbourIndices.Where(i => cells.GetValueOrDefault(i) != null);
        }

        protected void DumpDict<K, V>(Dictionary<K, V> dict) => dict.ToList().ForEach(t => Console.WriteLine($"{t.Key} => {t.Value}"));
        protected void DumpQueue<T>(Queue<T> queue) => Console.WriteLine("Queue: " + string.Join(", ",  queue.ToList().Select(t => $"{t}")));

        void iterate((int, int) N) {
            cellIndexQueue.Enqueue(N);
            DumpQueue(cellIndexQueue);

            visitedLookup[N] = true;

            (int, int) A;
            var adjacentCells = GetNeighbours(N, cellLookup);
            var unvisitedNeighbours = adjacentCells.Where(i => visitedLookup[i] == false).ToList();
            if (unvisitedNeighbours.Count != 0) {
                // Select a random neighbour
                A = unvisitedNeighbours[_random.Next(unvisitedNeighbours.Count)];
            }
            else {
                //  Continue to pop items off queue until we find a neightbour with at least 1 neighbour
                List<(int, int)> nUnivsitedNeighbours;
                do {
                    if (cellIndexQueue.Count == 0) return;
                
                    var n = cellIndexQueue.Dequeue();
                    nUnivsitedNeighbours = GetNeighbours(n, cellLookup).Where(i => visitedLookup[i] == false).ToList();
                } while(nUnivsitedNeighbours.Count == 0);
                
                A = nUnivsitedNeighbours[_random.Next(nUnivsitedNeighbours.Count)];
            }

            cellLookup[N].RemoveWall(cellLookup[A]);
            iterate(A);
        }

        Dictionary<(int, int), Cell> cellLookup = new  Dictionary<(int, int), Cell>();
        Dictionary<(int, int), bool> visitedLookup = new  Dictionary<(int, int), bool>();
        Queue<(int, int)> cellIndexQueue = new Queue<(int, int)>();

        public string GenerateMaze()
        {
            const int ROWS = 5;
            const int COLS = 5;

            cellLookup = CreateMazeCells(ROWS, COLS);
            visitedLookup = cellLookup.ToDictionary(t => t.Key, t => false);
            
            DumpDict(cellLookup);
            DumpDict(visitedLookup);

            var cellIndexQueue = new Queue<(int, int)>();
            DumpQueue(cellIndexQueue);
            Console.WriteLine("Creating maze.");
         
            
            var startIndex = (0, 0);
            iterate(startIndex);
            
            return "";
        }
    }

    class Program
    {
        IMazeAlgorithm algorithm;

        static void Main(string[] args)
        {
            var algorithm = new DepthFirstAlgorithm();
            algorithm.GenerateMaze();

            Console.WriteLine("Hello World!");
        }
    }
}

